from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver
from libcloud.compute.base import NodeImage
from libcloud.compute.drivers.dummy import DummyNodeDriver
from libcloud.compute.types import NodeState



AWS_EC2_ACCESS_ID = ''
AWS_EC2_SECRET_KEY = ''
AWS_REGION='us-west-2'

cls = get_driver(Provider.EC2)
driver = cls(AWS_EC2_ACCESS_ID, AWS_EC2_SECRET_KEY, AWS_REGION)

volumes = driver.list_volumes()

print(volumes)