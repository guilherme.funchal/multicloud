from django.db.models.signals import post_save, pre_save, pre_delete
from django.dispatch import receiver
from django.conf import settings
from amazon.models import INSTANCE, CREDENTIALS, VOLUMES, KEYS, SNAPSHOTS, IMAGES
from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver
from libcloud.compute.base import NodeImage
from datetime import datetime


import os.path
import logging
import datetime
import time

logger = logging.getLogger(__name__)

@receiver(pre_save, sender=INSTANCE)
def model_pre_save_instance(sender,  *args,  **kwargs):
    test = kwargs['instance'].id
    if test == None:
        AWS_REGION = kwargs['instance'].AWS_REGION
        KEY_ID = kwargs['instance'].KEY_ID_id
        print(AWS_REGION)
        print('id->',  KEY_ID)
        AMI_ID = kwargs['instance'].AMI_ID
        SIZE_ID = kwargs['instance'].SIZE_ID
        NAME = kwargs['instance'].NAME
        AWS_EC2_ACCESS_ID_id = kwargs['instance'].AWS_EC2_ACCESS_ID_id
        CREDENTIALS_OBJ = CREDENTIALS.objects.get(id = AWS_EC2_ACCESS_ID_id)
        KEY_OBJ = KEYS.objects.get(id = KEY_ID)
        print('KEYOBJ->', KEY_OBJ.NAME)
        cls = get_driver(Provider.EC2)
        driver = cls(CREDENTIALS_OBJ.id,  CREDENTIALS_OBJ.chave , AWS_REGION)
        sizes = driver.list_sizes()
        size = [s for s in sizes if s.id == SIZE_ID][0]
        image = NodeImage(id=AMI_ID, name=None, driver=driver)
        locations = driver.list_locations()
        for location in locations:
            if location.name == AWS_REGION:
                node = driver.create_node(name=NAME, image=image, size=size, location=location, ex_keyname=KEY_OBJ.NAME)
        time.sleep(30)
        kwargs['instance'].LAUCH_TIME = datetime.datetime.now()
        kwargs['instance'].LAST_REBOOT = datetime.datetime.now()
        resources = driver.list_nodes()
        for resource in resources:
            if resource.name == NAME and resource.public_ips:
                kwargs['instance'].IP = resource.public_ips[0]
                print(resource.id)
                kwargs['instance'].NODE_ID = resource.id
                kwargs['instance'].NODE_STATE_MAP = "Running"
            else:
                print('Node já existe...')


@receiver(pre_save, sender=KEYS)
def model_pre_save_keys(sender,  *args,  **kwargs):
#   print('Entrei na Keys')
    nome = kwargs['instance'].NAME
#   print('Entrei aqui->', nome)
    AWS_EC2_ACCESS_ID_id = kwargs['instance'].AWS_EC2_ACCESS_ID_id
    print(AWS_EC2_ACCESS_ID_id)
    CREDENTIALS_OBJ = CREDENTIALS.objects.get(id = AWS_EC2_ACCESS_ID_id)
    cls = get_driver(Provider.EC2)
    driver = cls(CREDENTIALS_OBJ.id,  CREDENTIALS_OBJ.chave)
    key_pair = driver.create_key_pair(name=nome)
#   print(key_pair.fingerprint)
    kwargs['instance'].FINGERPRINT = key_pair.fingerprint


@receiver(pre_save, sender=VOLUMES)
def model_pre_save_volumes(sender,  *args,  **kwargs):
    nome = kwargs['instance'].NAME
    instanceID = kwargs['instance'].NODE_ID
    size = kwargs['instance'].SIZE
    device = kwargs['instance'].DEVICE
    OBJ = INSTANCE.objects.get(NODE_ID = instanceID)
    local = OBJ.AWS_REGION
    CREDENTIALS_OBJ = CREDENTIALS.objects.get(id = OBJ.AWS_EC2_ACCESS_ID_id)
    cls = get_driver(Provider.EC2)
    driver = cls(CREDENTIALS_OBJ.id,  CREDENTIALS_OBJ.chave, region='us-east-1')
    locations = driver.list_locations()

    access_id = CREDENTIALS_OBJ.id

    print('Id do usuario->', access_id)

    for location in locations:
        print('location->', location.name)
        if str(location.name) == str(local):
            print('Achei o que foi pedido', location.name)
            volume = driver.create_volume(name=nome, size=size, location=location, ex_volume_type = 'gp2')
            kwargs['instance'].VOLUMEID = volume.id
            kwargs['instance'].CREATE_DATE = datetime.datetime.now()
            kwargs['instance'].AWS_REGION = location.name
            kwargs['instance'].AWS_EC2_ACCESS_ID_id = access_id

    instances = driver.list_nodes()
    time.sleep(15)
    volumes = driver.list_volumes()
    for instance in instances:
        if str(instanceID) == str(instance.id):
            for vol in volumes:
                 print('Volumes aqui->', vol)
                 if vol.id == volume.id:
                     attach = driver.attach_volume(instance, volume, device)
                     time.sleep(15)


@receiver(pre_save, sender=IMAGES)
def model_pre_save_images(sender,  *args,  **kwargs):
    print('Entrei aqui...')
    instanceID = kwargs['instance'].NODE_ID
    print('Numero da instancia->', instanceID)
    nome = kwargs['instance'].NAME
    print('Nome da imagem->', nome)
    instance = INSTANCE.objects.get(NODE_ID = instanceID)
    print('Numero da instancia->', instance)
    CREDENTIALS_OBJ = CREDENTIALS.objects.get(id = instance.AWS_EC2_ACCESS_ID_id)
    print('Credenciais->', CREDENTIALS_OBJ)
    cls = get_driver(Provider.EC2)
    driver = cls(CREDENTIALS_OBJ.id,  CREDENTIALS_OBJ.chave)
    instancia = str(instanceID)
    nomeImagem = 'copia_'+ instancia
    print('Nome da imagem->', nome)
    kwargs['instance'].IMAGE_ID = nomeImagem
    print('Image->', kwargs['instance'].IMAGE_ID)
    instances = driver.list_nodes()
    print(instances)

    for instance in instances:
        print('Instancia->', instance)

        if str(instance.id) == str(instancia):
            print('Criar imagem->', instance)
            print(instance.id)
            print('Criar imagem->', instance.id)
            image = driver.create_image(instance, nomeImagem)
            print('Criado ->', image)
            print('Criado ID ->',image.id)
            kwargs['instance'].IMAGE_ID = image.id


@receiver(pre_save, sender=SNAPSHOTS)
def model_pre_save_snapshots(sender,  *args,  **kwargs):
    volID = kwargs['instance'].NODE_ID
    print('VOL ID->', volID)
    snapshotNAME = kwargs['instance'].NAME
    print('Snapshot name->', snapshotNAME)
    VOL = VOLUMES.objects.get(VOLUMEID = volID)
    print('Vol->', VOL)
    id = VOL.AWS_EC2_ACCESS_ID_id
    print('ID->', id)
    instanceID = INSTANCE.objects.get(id = VOL.NODE_ID_id)
    print('instanceID->', instanceID)
    region = instanceID.AWS_REGION
    print('Regiao->', region)
    CREDENTIALS_OBJ = CREDENTIALS.objects.get(id = id)
    cls = get_driver(Provider.EC2)
    driver = cls(CREDENTIALS_OBJ.id,  CREDENTIALS_OBJ.chave)
    instances = driver.list_nodes()
    print('instancias->', instances)
    volumes = driver.list_volumes()
    print('volumes->', volumes)

    for volume in volumes:
        if str(volume.id) == str(volID):
            snapshot = driver.create_volume_snapshot(volume, name=snapshotNAME)
            snaps = driver.list_volume_snapshots(volume)
            print(snaps)
            for snap in snaps:
                if snap.name == snapshotNAME:
                    print('SNAP->', snap.id)
                    kwargs['instance'].SNAPSHOT_ID = snap.id


@receiver(pre_delete, sender=SNAPSHOTS)
def model_pre_delete_snapshots(sender,  *args,  **kwargs):
    volID = kwargs['instance'].NODE_ID
    snapshotNAME = kwargs['instance'].NAME
    VOL = VOLUMES.objects.get(VOLUMEID = volID)
    id = VOL.AWS_EC2_ACCESS_ID_id
    instanceID = INSTANCE.objects.get(id = VOL.NODE_ID_id)
    CREDENTIALS_OBJ = CREDENTIALS.objects.get(id = id)
    cls = get_driver(Provider.EC2)
    driver = cls(CREDENTIALS_OBJ.id,  CREDENTIALS_OBJ.chave)
    instances = driver.list_nodes()
    volumes = driver.list_volumes()
    for volume in volumes:
        if str(volume.id) == str(volID):
            snapshots = driver.list_volume_snapshots(volume)
            for snapshot in snapshots:
                if str(snapshot.name) == str(snapshotNAME):
                    test = driver.destroy_volume_snapshot(snapshot)


@receiver(pre_delete, sender=INSTANCE)
def model_pre_delete_instance(sender,  *args,  **kwargs):
    AWS_REGION = kwargs['instance'].AWS_REGION
    AWS_EC2_ACCESS_ID_id = kwargs['instance'].AWS_EC2_ACCESS_ID_id
    CREDENTIALS_OBJ = CREDENTIALS.objects.get(id = AWS_EC2_ACCESS_ID_id)
    cls = get_driver(Provider.EC2)
    driver = cls(CREDENTIALS_OBJ.id,  CREDENTIALS_OBJ.chave , AWS_REGION)
    nodes = driver.list_nodes()
    NAME = kwargs['instance'].NAME
    for node in nodes:
        if node.name == NAME:
            exc = node.id
            code = driver.destroy_node(node)


@receiver(pre_delete, sender=KEYS)
def model_pre_delete_keys(sender,  *args,  **kwargs):
    nome = kwargs['instance'].NAME
    AWS_EC2_ACCESS_ID_id = kwargs['instance'].AWS_EC2_ACCESS_ID_id
    print(AWS_EC2_ACCESS_ID_id)
    CREDENTIALS_OBJ = CREDENTIALS.objects.get(id = AWS_EC2_ACCESS_ID_id)
    cls = get_driver(Provider.EC2)
    driver = cls(CREDENTIALS_OBJ.id,  CREDENTIALS_OBJ.chave)
    key_pair = driver.list_key_pairs()
    key = driver.get_key_pair(nome)
    delete = driver.delete_key_pair(key)


@receiver(pre_delete, sender=VOLUMES)
def model_pre_delete_volumes(sender,  *args,  **kwargs):
    nome = kwargs['instance'].NAME
    instanceID = kwargs['instance'].NODE_ID
    size = kwargs['instance'].SIZE
    OBJ = INSTANCE.objects.get(NODE_ID = instanceID)
    local = OBJ.AWS_REGION
    CREDENTIALS_OBJ = CREDENTIALS.objects.get(id = OBJ.AWS_EC2_ACCESS_ID_id)
    cls = get_driver(Provider.EC2)
    driver = cls(CREDENTIALS_OBJ.id,  CREDENTIALS_OBJ.chave, region='us-east-1')
    locations = driver.list_locations()
    print(locations)
    volumes = driver.list_volumes()
    print(volumes)
    id = kwargs['instance'].VOLUMEID
    for volume in volumes:
        if volume.id == id:
            detach = driver.detach_volume(volume)
            time.sleep(15)
            delete = driver.destroy_volume(volume)

@receiver(pre_delete, sender=IMAGES)
def model_pre_delete_keys(sender,  *args,  **kwargs):
    print('Entrei aqui...')
    instanceID = kwargs['instance'].NODE_ID
    imageNAME = kwargs['instance'].IMAGE_ID
    OBJ = INSTANCE.objects.get(NODE_ID = instanceID)
    print('Obj->', OBJ.AWS_EC2_ACCESS_ID_id)
    CREDENTIALS_OBJ = CREDENTIALS.objects.get(id = OBJ.AWS_EC2_ACCESS_ID_id)
    cls = get_driver(Provider.EC2)
    driver = cls(CREDENTIALS_OBJ.id,  CREDENTIALS_OBJ.chave)
    instancia = str(instanceID)
    image = driver.get_image(imageNAME)
#   delete = driver.delete_image(image)



