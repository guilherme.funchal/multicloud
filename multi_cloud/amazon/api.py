from . import models
from . import serializers
from rest_framework import viewsets, permissions


class CREDENTIALSViewSet(viewsets.ModelViewSet):
    """ViewSet for the CREDENTIALS class"""

    queryset = models.CREDENTIALS.objects.all()
    serializer_class = serializers.CREDENTIALSSerializer
    permission_classes = [permissions.IsAuthenticated]


class INSTANCEViewSet(viewsets.ModelViewSet):
    """ViewSet for the INSTANCE class"""

    queryset = models.INSTANCE.objects.all()
    serializer_class = serializers.INSTANCESerializer
    permission_classes = [permissions.IsAuthenticated]



class VOLUMESViewSet(viewsets.ModelViewSet):
    """ViewSet for the INSTANCE class"""

    queryset = models.VOLUMES.objects.all()
    serializer_class = serializers.VOLUMESSerializer
    permission_classes = [permissions.IsAuthenticated]

class KEYSViewSet(viewsets.ModelViewSet):
    """ViewSet for the INSTANCE class"""

    queryset = models.KEYS.objects.all()
    serializer_class = serializers.KEYSSerializer
    permission_classes = [permissions.IsAuthenticated]

class IMAGESViewSet(viewsets.ModelViewSet):
    """ViewSet for the INSTANCE class"""

    queryset = models.IMAGES.objects.all()
    serializer_class = serializers.IMAGESSerializer
    permission_classes = [permissions.IsAuthenticated]

class SNAPSHOTSViewSet(viewsets.ModelViewSet):
    """ViewSet for the INSTANCE class"""

    queryset = models.SNAPSHOTS.objects.all()
    serializer_class = serializers.SNAPSHOTSSerializer
    permission_classes = [permissions.IsAuthenticated]