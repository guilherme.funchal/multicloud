from django.contrib import admin
from django import forms
from .models import CREDENTIALS, INSTANCE, VOLUMES, KEYS, IMAGES, SNAPSHOTS
from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver
from libcloud.compute.base import NodeImage
import datetime
import time

class CREDENTIALSAdminForm(forms.ModelForm):

    class Meta:
        model = CREDENTIALS
        fields = '__all__'

class VOLUMESAdminForm(forms.ModelForm):

    class Meta:
        model = VOLUMES
        fields = '__all__'

class KEYSAdminForm(forms.ModelForm):

    class Meta:
        model = KEYS
        fields = '__all__'

class SNAPSHOTSAdminForm(forms.ModelForm):

    class Meta:
        model = SNAPSHOTS
        fields = '__all__'


class IMAGESAdminForm(forms.ModelForm):

    class Meta:
        model = IMAGES
        fields = '__all__'

class CREDENTIALSAdmin(admin.ModelAdmin):
    form = CREDENTIALSAdminForm
    list_display = ['nome', 'id', 'chave']

admin.site.register(CREDENTIALS, CREDENTIALSAdmin)



class INSTANCEAdminForm(forms.ModelForm):

    class Meta:
        model = INSTANCE
        fields = '__all__'

def reboot(modeladmin, request, queryset):
    AWS_EC2_ACCESS_ID_id = queryset.values_list('AWS_EC2_ACCESS_ID_id', flat=True)[0]
    AWS_REGION = queryset.values_list('AWS_REGION', flat=True)[0]
    NODE_ID = queryset.values_list('NODE_ID', flat=True)[0]

    queryset.update(LAST_REBOOT=datetime.datetime.now())

    CREDENTIALS_OBJ = CREDENTIALS.objects.get(id = AWS_EC2_ACCESS_ID_id)
    cls = get_driver(Provider.EC2)
    driver = cls(CREDENTIALS_OBJ.id,  CREDENTIALS_OBJ.chave , AWS_REGION)
    nodes = driver.list_nodes()
    print('Reboot no node', NODE_ID)
    for node in nodes:
        if node.id == NODE_ID:
            reboot = node.id
            print(node.id)
            retorno = driver.reboot_node(node)
#           print(retorno)
reboot.short_description = "Reboot na instância"

class INSTANCEAdmin(admin.ModelAdmin):

    def has_change_permission(self, request, obj=None):
        return False

    form = INSTANCEAdminForm
    list_display = ['AWS_REGION', 'AMI_ID', 'SIZE_ID', 'NAME', 'NODE_STATE_MAP', 'LAST_REBOOT']
    readonly_fields = ['LAUCH_TIME', 'OWNER', 'IP', 'NODE_STATE_MAP','NODE_ID', 'LAST_REBOOT']
    actions = [reboot]

admin.site.register(INSTANCE, INSTANCEAdmin)

class VOLUMESAdmin(admin.ModelAdmin):

    def has_change_permission(self, request, obj=None):
        return False

    form = VOLUMESAdminForm
    list_display = ['VOLUMEID', 'SIZE', 'NAME', 'CREATE_DATE','DEVICE']
    readonly_fields = ['VOLUMEID', 'TYPE', 'SNAPSHOT', 'CREATE_DATE', 'IOPS', 'STATUS', 'AWS_REGION','AWS_EC2_ACCESS_ID_id']

admin.site.register(VOLUMES, VOLUMESAdmin)

class KEYSAdmin(admin.ModelAdmin):

    def has_change_permission(self, request, obj=None):
        return False

    form = KEYSAdminForm
    list_display = ['id', 'NAME']
    readonly_fields = ['id', 'FINGERPRINT']

admin.site.register(KEYS, KEYSAdmin)

class IMAGESAdmin(admin.ModelAdmin):

    def has_change_permission(self, request, obj=None):
        return False

    form = IMAGESAdminForm
    list_display = ['NAME','IMAGE_ID']
    readonly_fields = ['id', 'IMAGE_ID']

admin.site.register(IMAGES, IMAGESAdmin)

class SNAPSHOTSAdmin(admin.ModelAdmin):

    def has_change_permission(self, request, obj=None):
        return False

    form = SNAPSHOTSAdminForm
    list_display = ['NAME', 'SNAPSHOT_ID']
    readonly_fields = ['id', 'SNAPSHOT_ID']

admin.site.register(SNAPSHOTS, SNAPSHOTSAdmin)