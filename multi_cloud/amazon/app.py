from django.apps import AppConfig

class AmazonConfig(AppConfig):
    name = 'amazon'
    verbose_name = 'Serviços'

    def ready(self):
        # noinspection PyUnresolvedReferences
        import amazon.signals
