from django.views.generic import DetailView, ListView, UpdateView, CreateView

from .models import CREDENTIALS, INSTANCE, VOLUMES, SNAPSHOTS, IMAGES, KEYS
from .forms import CREDENTIALSForm, INSTANCEForm, VOLUMESForm, SNAPSHOTSForm, IMAGESForm, KEYSForm
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.dispatch import receiver

#import logging

#logger = logging.getLogger(__name__)
#logging.basicConfig(level=logging.DEBUG)

class CREDENTIALSListView(ListView):
    model = CREDENTIALS

class CREDENTIALSCreateView(CreateView):
    model = CREDENTIALS
    form_class = CREDENTIALSForm

class CREDENTIALSDetailView(DetailView):
    model = CREDENTIALS

class CREDENTIALSUpdateView(UpdateView):
    model = CREDENTIALS
    form_class = CREDENTIALSForm


class INSTANCEListView(ListView):
    model = INSTANCE

class INSTANCECreateView(CreateView):
    model = INSTANCE
    form_class = INSTANCEForm

class INSTANCEDetailView(DetailView):
    model = INSTANCE

class INSTANCEUpdateView(UpdateView):
    model = INSTANCE
    form_class = INSTANCEForm


class VOLUMESListView(ListView):
    model = VOLUMES

class VOLUMESCreateView(CreateView):
    model = VOLUMES
    form_class = VOLUMESForm

class VOLUMESDetailView(DetailView):
    model = INSTANCE

class VOLUMESUpdateView(UpdateView):
    model = VOLUMES
    form_class = VOLUMESForm


class KEYSListView(ListView):
    model = KEYS

class KEYSCreateView(CreateView):
    model = KEYS
    form_class = KEYSForm

class KEYSDetailView(DetailView):
    model = KEYS

class KEYSUpdateView(UpdateView):
    model = KEYS
    form_class = KEYSForm


class IMAGESListView(ListView):
    model = IMAGES

class IMAGESCreateView(CreateView):
    model = IMAGES
    form_class = VOLUMESForm

class IMAGESDetailView(DetailView):
    model = IMAGES

class IMAGESUpdateView(UpdateView):
    model = IMAGES
    form_class = IMAGESForm

class SNAPSHOTSListView(ListView):
    model = SNAPSHOTS

class SNAPSHOTSCreateView(CreateView):
    model = SNAPSHOTS
    form_class = SNAPSHOTSForm

class SNAPSHOTSDetailView(DetailView):
    model = SNAPSHOTS

class SNAPSHOTSUpdateView(UpdateView):
    model = SNAPSHOTS
    form_class = SNAPSHOTSForm